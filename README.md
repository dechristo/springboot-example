# Java Spring Boot + JPA + PostgreSQL example
This project uses Maven.

## 1. Install dependencies
In the project root folder type `mvn install`

To initialize the database with demo data use the script init_db.sql

## 2. Build and Run  the app
`mvn package && java -jar target/customer-0.0.1-DEV.jar`
 
## 3. Run the tests
Type `mvn test`
