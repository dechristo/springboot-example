package customer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTest {

	@Autowired
	private CustomerController controller;

    @Autowired
    private MockMvc mockMvc;
    
	@Test
	public void contexLoads() throws Exception {
		assertThat(controller).isNotNull();
	}
	
    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        this.mockMvc.perform(get("/health-check"))
        	.andDo(print())
        	.andExpect(status().isOk())
            .andExpect(content().json("{ \"msg\": \"All good!\" }"));
    }
    
    @MockBean
    private CustomerRepository customerRepository;

    @Test
    public void findByIdShouldReturnCustomerAsJson() throws Exception {
        Customer mockCustomer = new Customer(1984, "Orwell");
    	
        when(customerRepository.findById((long) 1984))
        	.thenReturn(Optional.of(mockCustomer));
        
        this.mockMvc.perform(get("/customer/1984"))
        	.andDo(print())
        	.andExpect(status().isOk())
            .andExpect(content().json("{\"id\":1984, \"name\":\"Orwell\"}"));
    }
    
    @Test
    public void findByIdShouldReturn404AndNotFoundMessageIfUserNotExists() throws Exception {
        when(customerRepository.findById((long) 66))
        	.thenReturn(null);
        
        this.mockMvc.perform(get("/customer/66"))
        	.andDo(print())
        	.andExpect(status().isNotFound());
    }
}