package customer;

import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {
			
	@Test
	public void testConstructorSetsAttributeIdCorrectly() {
		Customer customer = new Customer(777, "Skywalker");
		assertEquals(customer.getId(), 777);
	}
	
	@Test
	public void testConstructorSetsAttributeNameCorrectly() {
		Customer customer = new Customer(94, "Guiodai");
		assertEquals(customer.getName(), "Guiodai");
	}
	
	@Test
	public void testToStringReturnsCorrectString() {
		Customer customer = new Customer(844, "Annomander Rake");
		String customerStr = customer.toString();
		assertTrue(customerStr.equals("{\"id\":844, \"name\":\"Annomander Rake\"}"));
	}
}
