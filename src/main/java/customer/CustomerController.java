package customer;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {
	@Autowired
	CustomerRepository repository;
	
	@RequestMapping(value= "/health-check", produces= "application/json")
	public String healthCheck(){
		return "{ \"msg\": \"All good!\" }";
	}
	
	@RequestMapping(value= "/customer/{id}", method= RequestMethod.GET, produces= "application/json")
	public String findById(@PathVariable("id") long id){
		Optional<Customer> found = repository.findById(id);
		
		if (found != null && found.isPresent()) {
			Customer customer = found.get();
			return customer.toString();
		}
		throw new CustomerNotFoundException("Customer not found!");
	}
}
