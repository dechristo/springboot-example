package customer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "customer")
public class Customer {

    @Id
    private long id;
    private String name;

    protected Customer() {}

    public Customer(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public long getId() {
    	return this.id;
    }
    
    public String getName() {
    	return this.name;
    }

    @Override
    public String toString() {
        return String.format(
                "{\"id\":%d, \"name\":\"%s\"}",
                this.id, this.name);
    }

}