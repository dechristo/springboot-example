-- Table: public.customer

-- DROP TABLE public.customer;

CREATE TABLE public.customer
(
    id integer NOT NULL,
    name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT customer_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.customer
    OWNER to postgres;
	
INSERT INTO customer VALUES (1, 'Luke Skywalker');
INSERT INTO customer VALUES (2, 'Yoda');
INSERT INTO customer VALUES (3, 'Kilo Ren');
INSERT INTO customer VALUES (4, 'Kelly Slater'); 